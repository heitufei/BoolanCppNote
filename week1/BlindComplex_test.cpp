#include <iostream>
#include "BlindComplex.h"

int main() {
    Complex c1;
    Complex c2(3,2);
    Complex c3,c4;
    c3 += c2;
    c3 *= Complex(2,2);
    // cout << c1;
    // cout << c2;
    // cout << c3;
    // cout << "c2 = " << c2 << endl;
    cout << "c1 c2 = " << c1 << c2 << endl;
    cout << "c3 = " << c3 << endl;
    cout << (c3/= Complex(3,1)) << endl;
    cout << endl;
    return 0;
}
