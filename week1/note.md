## 一、类设计分类
### 1.1 Object Based
面对单一的类设计，类之间不存在关联
`描述的是类本身`
- class withou pointer member (不带指针成员)
*类中不包括指向外部数据、类、函数的引用，所有成员皆是以值的形式存在于类内部。类的函数功能不对外部数据造成影响。例如复数complex类。*
- class with pointer member (带指针成员)
*类中包括指向外部数据引用的成员。该成员的操作将影响外部数据本身。例如字符串string类*

### 1.2 Object Oriented
面对的是多重classes的设计，classes和classes之间存在关系
`描述的是类和类之间的关系`
- 未完待续
- 未完待续

## 二、类设计
### 类的文件组成形式

![c++代码基本形式.png](http://upload-images.jianshu.io/upload_images/565992-b1378a6835edc117.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

1. 头文件
    - 类声明
    - 数据结构声明
    - 可外见的define 、const 常量
2. 源文件

### 2.1 头文件的布局

![头文件的布局.png](http://upload-images.jianshu.io/upload_images/565992-1460364c9c7a6ebe.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)
- 前置声明-声明类声明时就应用到的类型-可声明当前类本身
    由此可见`class`就像一个类型，声明了一个`complex`变量，然后在` class complex {}`中初始化赋值
- 一般将类定义可置于`complex.cpp`文件中

### 2.2 头文件的防御式声明
借助条件编译构建防御式include

```
#ifndef __COMPLEX__  //如果没有定义
#define __COMPLEX__  //则定义
/*code*/
#endif //结束定义
```
### 2.3 类声明
```
class complex{
public:
  double real () const { return re; };
  double imag () const { return im; };
private:
  double re, im;
}
```
public 公有成员设计为可被外部访问
private 私有成员设计成不可被外部访问
一般将数据设计为私有成员，数据都是保密的。
一般将函数设计为公有成员，

## 三、函数设计
### 函数构成
`double real() const { return re; }`
- 函数名`real`
- 函数参数 空
- 返回值类型 double
- 函数体

### 参数类型选择

1. 传值value
参数复制了一份传进来的数据，作为函数体内的局部变量，函数对参数的改动将不会影响传外部的数据，函数结束后，参数变量消失。
2. 传引用
参数直接将外部数据的引用传进来，对引用的操作将直接影响外部数据。

设计原则 **能传引用传引用**
> 不让改变外部数据时，对引用类型的参数进行 const 修饰，这样就可以替换传值的方式

### 返回类型选择

1. 返回值
2. 返回引用

设计原则 **能返回引用就返回引用**
> 1. 返回的数据不在传进来的参数中时，只能通过新建临时变量来存储的结果，这时必须按值返回
> 2. 返回的数据可附在传进来的参数中时，返回引用，这个时候也可以返回void，因为对数据的改变已经通过参数反应到外部，但是仍然建议按引用的方式返回。
> ps: 2的前提是参数是引用传递，并且是没有const的

### 数据操作选择
在函数参数后加 `const` 的情况，函数定义中将不能对类的数据进行改动
