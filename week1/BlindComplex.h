// 根据流程重写一遍complex类
#ifndef __BLINDCOMPLEX__
#define __BLINDCOMPLEX__
class Complex;

class Complex
{
public:
    Complex(double re = 0, double im = 0) : re(re), im(im) {}

    Complex& operator += (const Complex& c);
    Complex& operator -= (const Complex& c);
    Complex& operator *= (const Complex& c);
    Complex& operator /= (const Complex& c);
    int real() const { return re; }
    int imag() const { return im; }
private:
    int re, im;
};

Complex&
Complex::operator += (const Complex& c)
{
    re += c.re;
    im += c.im;
    return *this;
}

Complex&
Complex::operator -= (const Complex& c)
{
    re -= c.re;
    im -= c.im;
    return *this;
}

Complex&
Complex::operator *= (const Complex& c)
{
    re *= c.re;
    im *= c.im;
    return *this;
}

Complex&
Complex::operator /= (const Complex& c)
{
    re /= c.re;
    im /= c.im;
    return *this;
}

#include <iostream>
using namespace std;
// 全局函数
ostream& operator << (ostream& os, const Complex& c)
{

    os << "(" << c.real() << "," << c.imag() << ")";
    // os << c.real();
    return os;
}

#endif
