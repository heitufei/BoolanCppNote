#include <iostream>
#include <cstdlib>

class Date;

using namespace std;

class Date
{
public:

	Date(int year = 1972, int month = 1, int day = 1) : year(year), month(month), day(day) {
		if (year < 0) {
			month = 12 - month;
			day = 31 - day;
		}
	}

	bool operator > (const Date& d);
	bool operator < (const Date& d);
	bool operator == (const Date& d);

	void print();
	long getDays () const { return year * 12 * 31 + month * 31 + day; }

private:

	int year, month, day;
};

inline void
Date::print()
{
	cout << year << "-" << month << "-" << day << endl;
}

inline bool
Date::operator > (const Date& d)
{
	if (this->getDays() > d.getDays()) {
		return true;
	} else {
		return false;
	}
}

inline bool
Date::operator < (const Date& d) {
	if (this->getDays() < d.getDays()) {
		return true;
	} else {
		return false;
	}
}

inline bool
Date::operator == (const Date& d) {
	if (d.getDays() == this->getDays()) {
		return true;
	} else {
		return false;
	}
}

// global function
Date daysToDate (int days)
{
	int year = int (days / (12 * 31));
	days = days % (12 * 31);
	int month = int (days / 31) + 1;
	days = days % 31;
	int day = days;
	return Date (year, month, day);
}

void CreatePoints(Date da[], int n)
{
	for (int i = 0; i < n; i++) {
		int days = rand() % (2100 * 12 * 31);
		da[i] = daysToDate(days);
	}
}

void sort(Date da[], int n)
{
	for (int i = 0; i < n; i++) {
		for (int j = i; j < n; j++) {
			if (da[i] > da[j]) {
				Date temp = da[i];
				da[i] = da[j];
				da[j] = temp;
			}
		}
	}
}

