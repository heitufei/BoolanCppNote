
/*
1. 分别给出下面的类型Fruit和Apple的类型大小（即对象size），
并通过画出二者对象模型图以及你的测试来解释该size的构成原因。pwd

2. 为上周题目中的 Fruit和Apple 添加 构造函数与 析构函数，
并在构造函数与析构函数中打印控制台信息，观察构造和析枸调用过程。
然后为Apple类重载::operator new和 ::operator delete，在控制台打印信息，
并观察调用结果。
 */

#include <iostream>
#include <cstdlib>
using namespace std;
class Fruit {
  int no;
  double weight;
  char key;
public:
  Fruit() {
    cout << "Fruit 的构造函数" << endl;
  }
  ~Fruit() {
    cout << "Fruit 的析构函数" << endl;
  }
  void* operator new (size_t size)
  {
    cout << "class new ... " << endl;
    return malloc(size);
  }
  void operator delete(void* ptr)
  {
    cout << "class free ..." << endl;
    return free(ptr);
  }
  void print() {   }
  virtual void process() {   }
};

class Apple: public Fruit {
  int size;
  char type;
public:
  Apple() {
    cout << "Apple 的构造函数" << endl;
  }
  ~Apple() {
    cout << "Apple 的析构函数" << endl;
  }
  void save() {   }
  virtual void process() {   }
};

void* operator new(size_t size)
{
  cout << "global new ... " << endl;
  return malloc(size);
}

void operator delete(void* ptr)
{
  cout << "global free ..." << endl;
  return free(ptr);
}

int main()
{
  cout << "----------------global new / delete ----------" << endl;
  cout << "Fruit:" << endl;
  Fruit* pF = new Fruit();
  delete pF;
  cout <<  "Apple:" << endl;
  Apple* pA = new Apple();
  delete pA;

  cout << "-----------------class new / delete ----------" << endl <<"\n\n";
  cout << "Fruit:" << endl;
  Fruit* pF2 = new Fruit();
  delete pF;
  cout << "Apple:" << endl;
  Apple* pA2 = new Apple();
  delete pA;
  return 0;
}
