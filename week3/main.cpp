/*
1. 分别给出下面的类型Fruit和Apple的类型大小（即对象size），
并通过画出二者对象模型图以及你的测试来解释该size的构成原因。pwd

2. 为上周题目中的 Fruit和Apple 添加 构造函数与 析构函数，
并在构造函数与析构函数中打印控制台信息，观察构造和析枸调用过程。
然后为Apple类重载::operator new和 ::operator delete，在控制台打印信息，
并观察调用结果。
 */
class Fruit {  // 32
  int no;  // 4
  double weight; // 8
  char key; // 1
public:
  void print() {   }
  virtual void process() {   }
};

class Apple: public Fruit {  // 40
  int size; // 4
  char type; // 1
public:
  void save() {   }
  virtual void process() {   }
};

#include <iostream>

using namespace std;

int main()
{
  cout << " int*  = " << sizeof(int*) << endl;
  cout << " int   = " << sizeof(int) << endl;
  cout << " double= " << sizeof(double) << endl;
  cout << " char  = " << sizeof(char) << endl;
  cout << " Fruit = " << sizeof(Fruit) << endl;
  cout << " Apple = " << sizeof(Apple) << endl;

  return 0;
}
