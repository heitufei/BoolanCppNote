class Fraction
{
public:
    Fraction(int num, int den=1)
    : m_numerator(num), m_denomination(den) {}
    /*
    operator double() const
    {
        return (double)(m_numerator / m_denomination);
    }*/
   
    Fraction operator +(const Fraction& f) const
    {
        int num = m_numerator*f.m_denomination + f.m_numerator*m_denomination;
        int den = m_denomination*f.m_denomination;
        return Fraction(num, den);
    }
private:
    int m_numerator;    //分子
    int m_denomination; //分母
};

#include <iostream>
using namespace std;

int main()
{
	Fraction f(3,5);
	Fraction d2 = f+4;	
	return 0;
}
