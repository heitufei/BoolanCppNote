# week 2

tag： boolan cpp

---
# String 类
## 需求

```
int main()
{
    String s1();        //1
    String s2("hello"); //2
    String s3(s1);      //3
    cout << s3 << endl; //4
    s3 = s2;            //5
    cout << s3 << endl;

    String* p = new String("hello2");
}
```

1. 没有初始值的构造函数
2. 有初始值的构造函数
3. 以s1为蓝本构造一个对象，拷贝构造函数
4. 输出，运算符重载
5. 拷贝赋值

## 拷贝构造、拷贝赋值、析构函数
Big three
> 1. 拷贝构造
> 2. 拷贝赋值
> 3. 析构函数

**类设计-接口设计**

```
class String
{
public:
	String(const char* cstr=0);
	String(const String& str);
	String& operator = (const String& str);
	~String();
	char* get_c_str() const { return m_data; }
private:
	char * m_data; //以字符串指针所指向的字符串作为数据，数据存储在堆中 new char[]
}
```

**拷贝**

> 1. 浅拷贝，只拷贝指针本身
> 2. 深拷贝，连指针所指内容一起拷贝
> 3. 默认的拷贝构造、拷贝赋值都是浅拷贝，在此不适用，因此重写成深拷贝功能。
> 4. 同理，因在析构函数中对深拷贝的内容进行销毁

**class with pointer members 必须有 拷贝构造 和 拷贝赋值**

> 1. 即将编译器提供的默认copy ctor 和 copy op= 重写为深拷贝，默认为浅拷贝；
> 2. 对象本身只具备值内容，例如 char* m_data, 对象只包括指针 m_data 的值,并不包含 m_data 所指向的字符串。



**析构函数销毁指针**

    指针 delete pointName;
    数组用 delete[] pointName;

```
~String()
{
        delete [] m_data;
        delete p;
}
```

**指针是局部变量则在函数结束前销毁**
```
int func()
{
    String p = new String("hello");
    ...
    delete p;
}
```

不进行深拷贝 s3 = s2，s3、s2 中的 m_data 将指向同一字符串，当在析构函数中销毁时，字符串被销毁两次，将造成不可控后果。

## 堆、栈与内存管理

进程在内存中的布局

栈stack
> 1. 函数
> 2. 局部非静态变量

堆heap
> 1. new 生成的对象，返回堆像的指针
> 2. 全局变量、全局静态变量 、静态局部变量


# 补充

## static
static data members 全局只有一份，
static function members 只有一份，没有 this 指针，只能处理静态数据
## 函数模板
## 类模板
