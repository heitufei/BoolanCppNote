#ifndef __RETANGLE__
#define __RETANGLE__
#include <iostream>
using namespace std;
class Rectangle;

class Shape
{
public:
	Shape(int n = 0): no(n) {}
	int getNo()const { return no;}
	void setNo(int n) { no = n; }
private:
	int no;
};
class Point
{
public:
	Point(int x = 0, int y = 0): x(x), y(y) {}
	int getX()const {return x;}
	int getY()const {return y;}
private:
	int x;
	int y;
};
class Rectangle: public Shape
{
	int width;
	int height;
	Point * leftUp;
public:
	Rectangle(){}
	Rectangle(int width, int height, int x, int y);
	Rectangle(const Rectangle& other);
	Rectangle& operator=(const Rectangle& other);
	~Rectangle();

	int getWidth() const { return width;}
	int getHeight() const {return height;}
	Point getLeftUp() const {return *leftUp;}
};

Rectangle::Rectangle(int width, int height, int x, int y)
	: Shape(), width(width), height(height), leftUp(new Point(x, y))
{

}

//提交版
/*
Rectangle::Rectangle(const Rectangle& other)
	: Shape(other.getNo()), width(other.width), height(other.height), leftUp(new Point(other.leftUp->getX(), other.leftUp->getY()))
{

}*/

Rectangle::Rectangle(const Rectangle& other)
	: Shape(other.getNo()), width(other.width), height(other.height)
{
	delete leftUp;
	leftUp = new Point(other.leftUp->getX(), other.leftUp->getY());
}

Rectangle&
Rectangle::operator = (const Rectangle& other)
{
	*this = other;
	leftUp = new Point(other.getLeftUp().getX(), other.getLeftUp().getY());
	return *this;
}
Rectangle::~Rectangle()
{
	delete leftUp;
}

void
printRetangle(const Rectangle& r)
{
	cout << "shape.no " << r.getNo() << endl
	     << "width 	  " << r.getWidth() << endl
	     << "height   " << r.getHeight() << endl
	     << "point.x  " << r.getLeftUp().getX() << endl
	     << "point.y  " << r.getLeftUp().getY() << endl;
}

#endif
