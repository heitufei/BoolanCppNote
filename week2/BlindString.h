// 根据流程重写一遍

#ifndef __BLINDSTRING__
#define __BLINDSTRING__

#include <cstring>

class String;

class String
{
public:
	String(const char* cstr = 0);
	String(const String& str);
	~String();

	String& operator = (const String& str);
	char* get_c_char () const { return m_data; }
private:
	char* m_data;
};

String::String(const char* cstr)
{
	if (cstr == 0) {
		m_data = new char[1];
		m_data[0] = '\0';
	} else {
		m_data = new char[strlen(cstr) + 1];
		strcpy(m_data, cstr);
	}
}

String::String(const String& str)
{
	m_data = new char[strlen(str.m_data) + 1];
	strcpy(m_data, str.m_data);
}

String::~String()
{
	delete [] m_data;
}

String&
String::operator = (const String& str)
{
	if (this == &str) {
		return *this;
	}

	delete [] m_data;
	m_data = new char[strlen(str.m_data) + 1];
	strcpy(m_data, str.m_data);

	return *this;
}

#include <iostream>
using namespace std;

ostream&
operator << (ostream& os, const String& str)
{
	os << str.get_c_char();
	return os;
}

// 写成：
// String&
// String::operator << (ostream os, const String& str)
// {
// 	os << str.get_c_char();
// 	return *this;
// }

#endif
